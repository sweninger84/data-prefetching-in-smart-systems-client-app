package at.weninger.prefetching.clientapp.Model;

/**
 * Created by Sabine on 16.02.2018.
 *
 * Wrapper class for the response, received from the server.
 * Contains fields for id and content.
 */
public class ResponseData {
    //set to -1 if an Error occurred, else this contains the RBL number
    private int id;
    private String content;

    public ResponseData(){}

    public ResponseData(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
