package at.weninger.prefetching.clientapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import at.weninger.prefetching.clientapp.Model.Message;
import at.weninger.prefetching.clientapp.Model.Position;
import at.weninger.prefetching.clientapp.Model.ResponseData;
import at.weninger.prefetching.clientapp.Network.NetworkStateReceiver;
import at.weninger.prefetching.clientapp.Network.TCPClient;

/**
 * Program entry point.
 * It is responsible for displaying all the input fields as well as the results.
 * The Layout xml files can be found under res/layout
 * It initializes the NetworkStateReceiver and TCPClient classes
 */
public class MainActivity extends AppCompatActivity {
    private String USERID = "111";
    private TCPClient mTcpClient;
    private BroadcastReceiver networkStateReceiver;

    private TextView response;
    private Gson gson = new GsonBuilder().create();
    private String messageJson;
    private PopupWindow pw;
    private List<ResponseData> resultsList = new ArrayList<>();
    private long timestamp;
    private int responseColumnCounter = 0;
    private LinearLayout row;
    private final int COLPERROW = 4;


    /**
     * Initialize and register NetworkStateReceiver to receive Network change Broadcast events
     * Initialize Results View
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        networkStateReceiver = new NetworkStateReceiver(this);
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(CONNECTIVITY_SERVICE);
        this.registerReceiver(networkStateReceiver, filter);

        response = (TextView) findViewById(R.id.response);
        response.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(networkStateReceiver);
    }

    /**
     * Creates a Popup Window to show result details
     * @param content: the text to be displayed
     */
    private void initiatePopupWindow(String content) {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.result_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            // create the PopupWindow
            pw = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            // display the popup in the center
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

            TextView resultText = (TextView) layout.findViewById(R.id.popup_detail_text);
            resultText.setText(content);
            resultText.setMovementMethod(new ScrollingMovementMethod());
            Button closeButton = (Button) layout.findViewById(R.id.popup_close);
            closeButton.setOnClickListener(closeButtonClickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener closeButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            pw.dismiss();
        }
    };

    /**
     * Retrieves and validates input data and wraps them in a new message object
     * @return the created message object
     */
    private Message createMessage(){
        boolean error = false;
        EditText editLat = (EditText) findViewById(R.id.editLat);
        EditText editLng = (EditText) findViewById(R.id.editLng);
        EditText editId = (EditText) findViewById(R.id.editId);

        Message message = new Message();
        Position destiny = new Position();
        try{
            destiny.setLat(Double.parseDouble(editLat.getText().toString()));
        }catch (IllegalArgumentException e){
            editLat.setError("Please enter a correct Latitude.");
            error = true;
        }
        try{
            destiny.setLon(Double.parseDouble(editLng.getText().toString()));
        }catch (IllegalArgumentException e){
            editLng.setError("Please enter a correct Longitude.");
            error = true;
        }
        if(error){
            return null;
        }
        message.setUserId(editId.getText().toString().isEmpty() ? USERID : editId.getText().toString());
        message.setDestinationPos(destiny);
        return message;
    }

    /**
     * Displays the response time (from sending of the message to the server to the receival of the response)
     * @param receiveTime: the time the response was received
     */
    private void showTime(long receiveTime){
        if(timestamp == 0)
            return;
        long difference = receiveTime - timestamp;
        response.setText(difference + " ms");
    }

    /**
     * Converts the json into a List of ResponseData objects and saves them
     * @param json: the response string in json format
     */
    private void saveResponse(String json){
        Type listType = new TypeToken<List<ResponseData>>() {}.getType();
        resultsList = gson.fromJson(json, listType);
    }

    /**
     * Displays each ResponseData object as a button on the UI.
     * The label of the button is the data id. Upon clicking the button an OverlayWindow with the
     * response is opened.
     */
    private void displayResponse(){
        LinearLayout resultsContainer = (LinearLayout) findViewById(R.id.main_results_view);
        System.out.println("Response Size: " + resultsList.size());
        for(ResponseData item : resultsList){
            if(item == null || item.getId() == -1 || item.getContent() == null){
                continue;
            }
            Button button = new Button(this);
            button.setText(String.valueOf(item.getId()));
            button.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            setOnClick(button, item.getContent());

            //Add a new row if necessary
            if(responseColumnCounter % COLPERROW == 0 || row == null){
                row = new LinearLayout(this);
                row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                row.addView(button);
                resultsContainer.addView(row);
            }else{
                row.addView(button);
            }
            responseColumnCounter++;

        }
    }

    private void setOnClick(final Button btn, final String msg){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow(msg);
            }
        });
    }

    /**
     * OnClick Handler for Button send
     * Calls createMessage() to get the messageJson to be sent.
     * Clears the resultsView
     * Starts and executes a new ConnectTask
     * @param view
     */
    public void sendMessage(View view){
        Message message = createMessage();
        if(message == null){
            return;
        }

        //Clear all previous responses
        resultsList.clear();
        response.setText("");
        LinearLayout resultsContainer = (LinearLayout) findViewById(R.id.main_results_view);
        resultsContainer.removeAllViews();
        responseColumnCounter = 0;

        //Connect to the server and send messageJson
        this.messageJson = gson.toJson(message);
        new connectTask().execute(this.messageJson);
    }

    /**
     * Starts a new ConnectTask using the previously (in sendMessage method) generated messageJson
     */
    public void resendMessage(){
        if(messageJson != null){
            new connectTask().execute(messageJson);
        }
    }

    /**
     * Class extending AsyncTask for sending the user metadata to the server and waiting for a
     * response while keeping the UI responsive.
     */
    public class connectTask extends AsyncTask<String,String,TCPClient> {

        /**
         * Initializes a new TCPClient, sets the message to be sent and executes it
         * @param message: the message to be sent
         * @return a new TCPClient object
         */
        @Override
        protected TCPClient doInBackground(String... message) {

            //we create a TCPClient object handing it a new OnMessageReceived object
            mTcpClient = new TCPClient(new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method calls the onProgressUpdate
                    publishProgress(message);
                }
            });
            timestamp = System.currentTimeMillis();
            mTcpClient.setMessageToBeSent(message[0]);
            mTcpClient.run();

            return null;
        }

        /**
         * Called upon receipt of a response from the server.
         * Displays the response time as well as the response.
         * @param values: the response
         */
        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            System.out.println("Progress update: " + values[0]);
            showTime(System.currentTimeMillis());
            saveResponse(values[0]);
            displayResponse();

            if(mTcpClient != null){
                System.out.println("Stopping the client");
                mTcpClient.stopClient();
            }
        }
    }
}
