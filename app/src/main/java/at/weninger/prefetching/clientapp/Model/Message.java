package at.weninger.prefetching.clientapp.Model;

/**
 * Created by Sabine on 13.01.2018.
 *
 * Wrapper class for the user metadata which are sent to the server
 */
public class Message {
    private String userId;
    private Position currentPos;
    private Position destinationPos;
    private int userType;
    // in our case the id of the last device the user encountered
    private int directionInfo = -1;
    private double movementSpeed = -1; //in m/sec

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Position getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(Position currentPos) {
        this.currentPos = currentPos;
    }

    public Position getDestinationPos() {
        return destinationPos;
    }

    public void setDestinationPos(Position destinationPos) {
        this.destinationPos = destinationPos;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getDirectionInfo() {
        return directionInfo;
    }

    public void setDirectionInfo(int directionInfo) {
        this.directionInfo = directionInfo;
    }

    public double getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(double movementSpeed) {
        this.movementSpeed = movementSpeed;
    }
}
