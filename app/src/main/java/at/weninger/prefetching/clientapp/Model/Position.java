package at.weninger.prefetching.clientapp.Model;

/**
 * Created by Sabine on 13.01.2018.
 *
 * GPS Position wrapper
 */
public class Position {
    private double lat;
    private double lon;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
