package at.weninger.prefetching.clientapp.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import at.weninger.prefetching.clientapp.MainActivity;


/**
 * Created by Sabine on 18.01.2018.
 *
 * This class triggers the sending of the user metadata to each new IoT device
 * that the mobile device connects to.
 * It extends the Android BroadcastReceiver class and is notified by the Android system
 * when a change in network state occurs.
 */
public class NetworkStateReceiver extends BroadcastReceiver{
    private final int COOLDOWN = 300; //5 min
    private String TAG = "NetworkStateReceiver";
    private String lastNetwork = "";
    private Long timestamp;
    private MainActivity mainActivity;

    public NetworkStateReceiver(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    /**
     * Upon connecting to a new network, the method checks whether this device has been previously
     * (within the last 5 minutes => COOLDOWN) connected to this network.
     * In that case, the user metadata have already been sent to this device previously
     * and no action needs to be taken now.
     * Otherwise, the sending of a new message to the connected device is triggered.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Network connectivity change");

        if (intent.getExtras() != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

            if (ni != null && ni.isConnectedOrConnecting()) {
                String currentNetworkName = ni.getExtraInfo();
                Log.i(TAG, "Network " + currentNetworkName + " connected");
                if(lastNetwork.equals(currentNetworkName) && timestamp != null && timestamp > (System.currentTimeMillis()/1000 - COOLDOWN)){
                    //We already sent out info to this network
                    return;
                }
                lastNetwork = currentNetworkName;
                timestamp = System.currentTimeMillis()/1000;
                //send message to newly connected device
                mainActivity.resendMessage();
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                Log.d(TAG, "There's no network connectivity");
            }
        }
    }
}
