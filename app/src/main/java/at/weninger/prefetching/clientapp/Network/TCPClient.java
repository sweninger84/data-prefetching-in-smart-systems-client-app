package at.weninger.prefetching.clientapp.Network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Sabine on 19.12.2017.
 *
 * This class opens the socket to the IoT device application,
 * sends the metadata and returns the response to the MainActivity class.
 */
public class TCPClient {
    private String serverAnswer;
    private String messageToBeSent;
    public static final String SERVERIP = "192.168.1.1";
    public static final int SERVERPORT = 6666;
    private OnMessageReceived messageListener = null;
    private boolean mRun = false;

    PrintWriter out;
    BufferedReader in;

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(OnMessageReceived listener) {
        messageListener = listener;
    }

    public void stopClient(){
        mRun = false;
    }

    /**
     * Opens a socket to the fixed IP address (SERVERIP and SERVERPORT), sends the metadata
     * received from the Main Activity class and waits for an answer from the server.
     * The server answer is then passed back to the Main Activity class via the messageListener object
     */
    public void run() {

        mRun = true;

        try {
            InetAddress serverAddr = InetAddress.getByName(SERVERIP);

            Log.e("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVERPORT);

            try {
                //send the message to the server
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                if(messageToBeSent != null){
                    Log.e("TCPClient", "Sending message: " + messageToBeSent);
                    out.println(messageToBeSent);
                    out.flush();
                    Log.e("TCPClient", "Message sent");
                }

                //receive the message which the server sends back
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //client listens for the messages sent by the server
                while (mRun) {
                    serverAnswer = in.readLine();
                    if (serverAnswer != null && messageListener != null) {
                        //call the method messageReceived implemented in the MyActivity class
                        messageListener.messageReceived(serverAnswer);
                    }
                    serverAnswer = null;

                }
            } catch (Exception e) {
                Log.e("TCP", "S: Error", e);
            } finally {
                socket.close();
            }
        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }
    }

    public void setMessageToBeSent(String messageToBeSent) {
        this.messageToBeSent = messageToBeSent;
    }

    /**
     * Interface for the message received listener.
     * The method messageReceived(String message) is implemented in the MainActivity class
     * at on asynckTask doInBackground method.
     */
    public interface OnMessageReceived {
        void messageReceived(String message);
    }
}
