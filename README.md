# Android App for Data Prefetching in Smart Systems #

For a description of the project see: https://bitbucket.org/sweninger84/data-prefetching-in-smart-systems

This app provides a interface for the user to put in data (their GPS-coordinates). These data are then sent to the connected IoT device (IP address: 192.168.1.1) and the returned data are displayed.

Returned data are live public transport timetable information from the Wiener Linien API. Upon connecting to a new network, the data will be resent.

For testing purposes this app also measures the time in ms from sending the request upon receipt of the response and displays this information.